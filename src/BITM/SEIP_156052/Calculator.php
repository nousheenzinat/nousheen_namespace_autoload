<?php

namespace App;


class Calculator
{

    private $number1;
    private $number2;
    private $operation;
    private  $result;

    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function getNumber1()
    {
        return $this->number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function getNumber2()
    {
        return $this->number2;
    }
    public function setResult()
    {
         switch($this->operation){

             case  "Addition":
                        $this-> result = $this->doAddition();
                        break;


         }
    }

    public function getResult()
    {   $this->setResult();
        return $this->result;
    }
  public function doAddition($number1,$number2){

      return $number1+$number2;


  }
    public function doSubtraction($number1,$number2){

        return $number1-$number2;


    }
    public function doMultiplication($number1,$number2){

        return $number1*$number2;


    }
    public function doDivision($number1,$number2){

        return $number1/$number2;


    }
}