<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 9:41 AM
 */

namespace Tap;



use App\Person;

class student extends  Person
{
   private $studentid;

    public function setStudentid($studentid)
    {
        $this->studentid = $studentid;
    }

    public function getStudentid()
    {
        return $this->studentid;
    }
}